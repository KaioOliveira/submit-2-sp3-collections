import {RENDER_LIST, ADD_TO_COLLECTION, REMOVE_FROM_COLLECTION} from './action-types'

export const renderList = (characters) => ({
  type: RENDER_LIST,
  characters:characters,
});
export const addToCollection=(characters)=>({
    type:ADD_TO_COLLECTION,
    characters:characters
})
export const removeFromCollection=(characters)=>({
    type:REMOVE_FROM_COLLECTION,
    characters:characters
})

const defaultState = [];

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case "RENDER_LIST":
      return action.characters
    case "ADD_TO_COLLECTION":
      return action.characters
    case "REMOVE_FROM_COLLECTION":
        return action.characters  
    default:
      return state;
  }
};
export default reducer;
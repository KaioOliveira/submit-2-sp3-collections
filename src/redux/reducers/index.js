import { combineReducers } from "redux";

import characters from "../reducers/characters"

export default combineReducers({ characters });
import React from "react";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import { Home, Collection, Chart } from "./pages/routes";
import { MdCollections } from "react-icons/md";
import { BsPeopleFill } from "react-icons/bs";
import { FaChartPie } from "react-icons/fa";
import { motion } from "framer-motion";
import {
  TopBarLinks,
  TopBar,
  StyledLink,
} from "./components/styled-components";

function App() {

  return (
    <div className="App">
      <TopBar>
        <TopBarLinks>
          <motion.div whileHover={{ scale: 1.3 }} whileTap={{ scale: 0.8 }}>
            <StyledLink to="/collection">
              <MdCollections />
            </StyledLink>
          </motion.div>
          <motion.div whileHover={{ scale: 1.3 }} whileTap={{ scale: 0.8 }}>
            <StyledLink to="/">
              <BsPeopleFill />
            </StyledLink>
          </motion.div>
          <motion.div whileHover={{ scale: 1.3 }} whileTap={{ scale: 0.8 }}>
            <StyledLink to="/chart">
              <FaChartPie />
            </StyledLink>
          </motion.div>
        </TopBarLinks>
      </TopBar>

      <Switch>
        <Route exact path="/">
          <Home/>
        </Route>
        <Route path="/collection">
          <Collection/>
        </Route>
        <Route path="/chart">
          <Chart/>
        </Route>
      </Switch>
    </div>
  );
}

export default App;

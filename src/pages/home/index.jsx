import React, { useState, useEffect } from "react";
import Pokemon from "../../components/pokemon/index";
import RickyAndMorty from "../../components/ricky-and-morty/index";
import { notification } from "antd";
import Axios from "axios";
import "./index.css";
import { DefaultButton } from "../../components/styled-components";
import { useSelector, useDispatch } from "react-redux";
import {addToCollection} from "../../redux/actions/index"
const Home = () => {
  const characters = useSelector((state) => state.characters);
  const dispatch = useDispatch()
  const [nameRenderList, setNameRenderList] = useState("ricky-and-morty");
  const [data, setData] = useState([]);
  const handleOnSelect = (newCharacter) => {
    const alreadyAdd = characters.some(
      ({ name }) => name === newCharacter.name
    );

    if (alreadyAdd) {
      return notification["error"]({
        placement: "topRight",
        getContainer: () => document.body,
        key: newCharacter.name,
        message: "Erro",
        description: "Personagem já foi adicionado!",
      });
    } else {
      notification.success({
        getContainer: () => document.body,
        placement: "topRight",
        key: newCharacter.name,
        message: "Sucesso",
        description: "Personagem adicionado!",
      });

      dispatch(addToCollection([...characters, newCharacter]));
    }
  };

  const requestPokemonData = () => {
    setNameRenderList("pokemon");
    Axios.get("https://pokeapi.co/api/v2/pokemon?limit=20").then((res) => {
      setData(res.data.results);
    });
  };
  const renderRickyAndMortyPage = () => {
    setNameRenderList("ricky-and-morty");
    Axios.get("https://rickandmortyapi.com/api/character/").then((res) => {
      setData(res.data.results);
    });
  };
  useEffect(() => {
    renderRickyAndMortyPage();
  }, []);
  return (
    <div className="content">
      {nameRenderList === "ricky-and-morty" ? (
        <DefaultButton className="default-button" onClick={requestPokemonData}>
          Pokemon
        </DefaultButton>
      ) : (
        <DefaultButton
          className="default-button"
          onClick={renderRickyAndMortyPage}
        >
          Rick and Morty
        </DefaultButton>
      )}
      {nameRenderList === "ricky-and-morty" ? (
        <RickyAndMorty data={data} onSelect={handleOnSelect} />
      ) : (
        <Pokemon data={data} onSelect={handleOnSelect} />
      )}
    </div>
  );
};

export default Home;

import React from "react";
import { Pie } from "react-chartjs-2";
import { useSelector } from "react-redux";

const Chart = () => {
  const characters = useSelector((state) => state.characters);
  const charactersData = characters.reduce((current, { type }) => {
    current[type] ? (current[type] += 1) : (current[type] = 1);
    return current;
  }, {});
  console.log(charactersData);
  const data = {
    labels: Object.keys(charactersData),
    datasets: [
      {
        data: Object.values(charactersData),
        backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
        hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
      },
    ],
  };
  return (
    <div>
      <Pie data={data} />
    </div>
  );
};

export default Chart;

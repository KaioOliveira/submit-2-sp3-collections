import React, { useState } from "react";
import { notification } from "antd";
import { DefaultButton } from "../../components/styled-components";
import CharacterList from "../../components/character-list/index";
import { useSelector, useDispatch } from "react-redux";
import { removeFromCollection } from "../../redux/actions";
const Collection = () => {
  const dispatch = useDispatch()
  const characters = useSelector((state) => state.characters);
  const [nameRenderList, setNameRenderList] = useState("ricky-and-morty");
  const renderPokemonPage = () => {
    setNameRenderList("pokemon");
    return <CharacterList characters={characters} />;
  };
  const renderRickyAndMortyPage = () => {
    setNameRenderList("ricky-and-morty");
    return;
  };
  const handleOnSelect = ({ name }) => {
    notification.info({
      key: name,
      message: "Boa!",
      description: "Personagem removido!",
    });
    dispatch(removeFromCollection(characters.filter((character) => character.name !== name)));
  };

  return (
    <div className="content">
      {nameRenderList === "ricky-and-morty" ? (
        <DefaultButton className="default-button" onClick={renderPokemonPage}>
          Pokemon
        </DefaultButton>
      ) : (
        <DefaultButton
          className="default-button"
          onClick={renderRickyAndMortyPage}
        >
          Rick and Morty
        </DefaultButton>
      )}
      <CharacterList
        characters={characters.filter(
          (character) => character.type === nameRenderList
        )}
        header="Coleção de cards"
        onSelect={handleOnSelect}
      />
    </div>
  );
};
export default Collection;

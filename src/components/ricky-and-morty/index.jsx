import React from "react";
import CharacterList from "../character-list/index";

const RickyAndMorty = ({ data, onSelect }) => {
  const charactersData = data.map((character) => {
    character.type = "ricky-and-morty";
    return character;
  });
  return (
    <div>
      <CharacterList characters={charactersData} onSelect={onSelect} />
    </div>
  );
};
export default RickyAndMorty;

import React from "react";
import CharacterList from "../character-list/index";
const Pokemon = ({ data, onSelect }) => {
  const pokemonData = data.map((pokemon) => {
    const brokenUrl = pokemon.url.split("/");
    const id = brokenUrl[brokenUrl.length - 2];
    const name = pokemon.name;
    const image =
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
      id +
      ".png";
    return { name: name, image: image, type: "pokemon" };
  });

  return (
    <CharacterList
      characters={pokemonData}
      onSelect={onSelect}
    />
  );
};
export default Pokemon;
